defmodule Part do
  @moduledoc """
  Represente a universal part.
  """

  @currency "#,##0.00 [$€-40C];[RED]-#,##0.00 [$€-40C]"

  defstruct [
    rohs: false,
    description: nil,
    datasheet: nil,
    datasheet_link: nil,
    manufacturer: nil,
    ref_manufacturer: nil,
    manufacturer_link: nil,
    provider: "Unknown",
    provider_link: "https://...",
    ref_provider: nil,
    ref_provider_link: nil,
    stock: -1,
    currency: 1,
    quotation: [],
    freight: [],
    teams: []
  ]

  def init(name, content) do
    provider = case content do
        %{"Fournisseur" => provider} -> provider
        %{"lcsc#" => _} -> "lcsc"
    end
    provider = provider |> String.downcase
    ref_provider = case content do
        %{"RefFournisseur" => ref_provider} -> ref_provider
        %{"lcsc#" => ref_provider} -> ref_provider
    end
    asked = case content do
        %{"Qté" => asked} -> asked
        %{"Qty" => asked} -> asked
        %{"Quantity" => asked} -> asked
        %{"qty" => asked} -> asked
        _ -> 1
    end

    %Part {
      provider: provider,
      ref_provider: ref_provider,
      teams: %{ name: name, asked: asked }
    }
  end
  
  def factorize_teams(%Part{} = part) do
    %{part | teams: part.teams
           |> Enum.group_by(fn team -> team.name end)
           |> Enum.map(fn {name, multeams} -> %{
                asked: multeams |> Enum.map(fn team -> team.asked end)
                                |> Enum.sum(),
                name: name
              } end)
    }
  end

  def solve_optimized_price(%Part{} = part) do
    asked = Part.solve_required_quantity(part)

    quotation = part.quotation
        |> Enum.min_by(fn price ->
                          Enum.max([asked, price.qty]) * price.origin
                       end)
    %{
        origin: quotation.origin,
        asked: asked,
        askable: Enum.max([asked, quotation.qty])
    }
  end

  def solve_required_quantity(%Part{} = part) do
    part.teams |> Enum.map(fn %{asked: quantity} -> quantity end) 
               |> Enum.sum()
  end

  def to_xls(%Part{} = part, nameteams) do
    selected_quotation = Part.solve_optimized_price(part)
    askers = nameteams |> Enum.map(fn name ->
        team = part.teams |> Enum.find(fn team -> team.name == name end)
        case team do
          nil -> 0
          team -> team.asked
        end
    end)

    line = Enum.concat([
        [{:formula, "INDIRECT(ADDRESS(ROW(), COLUMN()+1))" <> "*" <>
                    "INDIRECT(ADDRESS(ROW(), COLUMN()+2))"},
               num_format: @currency],
        [selected_quotation.origin, num_format: @currency],
        selected_quotation.askable,
    ], askers)
    line = Enum.concat(line, [
        [part.rohs == true, num_format: "BOOLEAN"],
        part.description,
        [{:formula, "HYPERLINK(\"" <> part.datasheet_link <> "\", \""
                                   <> part.datasheet <> "\")"}],
        [{:formula, "HYPERLINK(\"" <> part.manufacturer_link <> "\", \""
                                   <> part.manufacturer <> "\")"}],
        part.ref_manufacturer,
        [{:formula, "hyperlink(\"" <> part.provider_link <> "\", \""
                                   <> part.provider <> "\")"}],
        [{:formula, "hyperlink(\"" <> part.ref_provider_link <> "\", \""
                                   <> part.ref_provider <> "\")"}]
    ])

    line
  end
end
