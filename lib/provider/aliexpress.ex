defprotocol Aliexpress do
  def init(part)
  def request_freight(part)
  def request_freight(part, ref_provider)
  def request_product(part)
end

defimpl Aliexpress, for: Part do
  @moduledoc """
  Represente a AliExpress's part.
  """

  @fournisseur "AliExpress"
  @fournisseur_link "https://fr.aliexpress.com"

  def init(%Part{} = part) do
    part = %{part | provider: @fournisseur}
    part = %{part | provider_link: @fournisseur_link}
    part
  end

  def request_freight(%Part{} = part) do
    case Mojito.get(part.provider_link
                 <> "/aeglodetailweb/api/logistics/freight?productId="
                 <> part.ref_provider
                 <> "&count=1&country=FR", [
        {"referer", "https://fr.aliexpress.com/item/"
                 <> part.ref_provider
                 <> ".html"},
        {"cookie", "aep_usuc_f=c_tp=EUR;aep_usuc_f=c_tp=EUR"}
    ]) do
      {:error, %Mojito.Error{message: nil, reason: :timeout}} -> 
         part.ref_provider_link |> IO.puts
      {:ok, response} ->
         freight = response |> Map.get(:body)
                            |> Jason.decode!
         freight = freight["body"]["freightResult"]
                 |> Enum.map(fn elem -> %{
                    time: elem["time"],
                    tracking: elem["tracking"],
                    company: elem["company"],
                    value: elem["freightAmount"]["value"],
                    currency: elem["freightAmount"]["currency"],
                    value2: elem["previewFreightAmount"]["value"],
                    currency2: elem["previewFreightAmount"]["currency"]
                } end)
         part = %{part | freight: freight}
         part
    end
  end

  def request_freight(%Part{} = part, ref_provider) do
    part = %{part | ref_provider: ref_provider}
    part = %{part | ref_provider_link: part.provider_link <> "/item/" <>
                                        part.ref_provider <> ".html"}
    part = Aliexpress.request_freight(part)
    part
  end

  @doc """
  Update the product information.

  This function returns a copy of part with a updated product information
  # Examples
  ```
  iex> part = %Part{ref_provider: "33045727417"}
  iex> Aliexpress.request_product(part)
  ```
  """
  def request_product(%Part{} = part) do
    part = %{part | ref_provider_link: part.provider_link <> "/item/" <>
                                        part.ref_provider <> ".html"}
    case Mojito.get(part.ref_provider_link) do
      {:error, %Mojito.Error{message: nil, reason: :timeout}} -> 
         part.ref_provider_link |> IO.puts
      {:ok, response} ->
        {:ok, document} = response |> Map.get(:body)
                                   |> Floki.parse_document
        [{_, _, script} | _] =
            document |> Floki.find("script:fl-contains('runParams')")
        run = script |> Floki.text
                     |> String.replace(~r/\r|\n/, "")

                     |> String.replace(~r/window.runParams = /, "")
                     |> String.replace(~r/};.*/, ~S"}")

                     |> String.replace(~r/data:/, ~S"\"data\":")
                     |> String.replace(~r/csrfToken:/, ~S"\"csrfToken\":")
                     |> String.replace(~r/abVersion:/, ~S"\"abVersion\":")
                     |> String.replace(~r/abtestMap:/, ~S"\"abtestMap\":")

                     |> String.replace(~r/: '/, ~S": \"")
                     |> String.replace(~r/',/, ~S"\",")
         run = run |> Jason.decode!
    
         description = run["data"]["titleModule"]["subject"]
         part = %{part | description: description}
         part = %{part | rohs: description |> String.contains?("RoHS") || NULL}

         part = %{part | manufacturer: run["data"]["storeModule"]["storeName"]}
         part = %{part | manufacturer_link:
            Enum.join([part.provider_link, "/store/",
                       run["data"]["storeModule"]["storeNum"]])}
         [ref_manufacturer | _] = run["data"]["specsModule"]["props"]
                        |> Enum.map(fn prop -> prop["attrValue"] end)
         part = %{part | ref_manufacturer: ref_manufacturer}

         part = %{part | datasheet: ref_manufacturer}
         part = %{part | datasheet_link: 
            Enum.join([part.ref_provider_link, "#product-description"])}

         stock = run["data"]["quantityModule"]["totalAvailQuantity"]
         part = %{part | stock: stock}
        
         qty = run["data"]["priceModule"]["numberPerLot"]
         origin = run["data"]["priceModule"]["maxAmount"]["value"]
         discount = run["data"]["priceModule"]["maxActivityAmount"]["value"]
         discount = if is_nil(discount) do origin else discount end
         part = %{part | quotation: [%{
            qty: qty,
            discount: discount / qty,
            origin: origin / qty
         }]}
         part
    end
  end
end

defprotocol AliexpressSet do
  def paypal(provider)
end

defimpl AliexpressSet, for: Provider do
  @moduledoc """
  Represente the AliExpress's provider.
  """

  # Frequently asked questions -
  # https://www.paypal.com/c2/webapps/mpp/sell-on-aliexpress-with-paypal
  #
  # How much marketplace fee do buyers need to pay when they pay
  # with PayPal on AliExpress?
  #
  # When the transaction amount is equal to or less than $20 USD,
  # the marketplace fee is $0.80 per transaction.
  # When the transaction amount is more than $20 USD,
  # the marketplace fee is 4% of the transaction amount.
  @fee 0.80
  @limit 20
  @fee20 0.4
  def paypal(%Provider{} = provider) do
    fee = @fee
    limit = @limit
    fee20 = @fee20
    provider =
      %{provider | card: provider.card
                 |> Enum.map(fn part ->
                      quotation = Part.solve_optimized_price(part)
                      price = quotation.askable * quotation.origin
                      extra = if price < limit do fee else price/100*fee20 end

                      %{
                        rohs: true,
                        description: "AliExpress charges a fee to use PayPal",
                        provider: "PayPal",
                        provider_link: "https://www.paypal.com/fr",
                        manufacturer: part.manufacturer,
                        ref_manufacturer: part.ref_manufacturer,
                        manufacturer_link: part.manufacturer_link,
                        teams: part.teams,
                        quotation: [%{
                          qty: 1,
                          origin: extra
                        }]
                      }
                    end)}
    provider
  end
end
