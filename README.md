# Bon42chips
[![Build Status](https://42chips/compta.svg)](https://42chips/compta)

```
mix order42 -i group_a.csv -i group_b.csv -o demande_de_depense.xls
```

## Knowledge
* [Distributors of Electronic Component Parts](https://octopart.com/distributors)

https://lcsc.com/api/orders/checkout/getExpresses?country=FR&weight=1233
Handling Fee - https://lcsc.com/faqs/show?id=12
