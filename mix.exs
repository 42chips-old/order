defmodule Order42.MixProject do
  use Mix.Project

  @version "0.1.0"

  def project do
    [
      app: :order42,
      version: @version,
      elixir: "~> 1.10",
      deps: deps(),
      package: package(),
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      docs: docs(),
      name: "Order42",
      source_url: "https://gitlab.com/42chips/order",
      homepage_url: "https://42chips.gitlab.io/order",
      description: "set a order form from bill of materials (BOM) list",
      aliases: aliases(),
      escript: escript(),
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.22", only: :dev, runtime: false},
      {:ex_cli, "~> 0.1.0"},
      {:csv, "~> 2.4"},
      {:castore, "~> 0.1.0"},
      {:mojito, "~> 0.7.1"},
      {:jason, "~> 1.2"},
      {:floki, "~> 0.28.0"},
      {:elixir_map_to_xml, "~> 0.1.0"},
      {:plug_minify_html, "~> 0.1.0"},
      {:elixlsx, "~> 0.4.2"},
      {:benchee, "~> 1.0", only: :dev},
      {:benchee_html, "~> 1.0", only: :dev},
    ]
  end

  defp package do
    [
      maintainers: ["adjivas"],
      links: %{gitlab: "https://gitlab.com/42chips/order"},
      files: ~w(lib config mix.exs README.md)
    ]
  end

  defp docs do
    [
      main: "Order42",
      source_ref: "v#{@version}",
      logo: "resources/logo.jpg",
      extras: ["README.md"]
    ]
  end

  defp aliases do
    [
      build: [
        "docs",
      ]
    ]
  end

  defp escript do
    [
      main_module: Order42.CLI
    ]
  end
end
