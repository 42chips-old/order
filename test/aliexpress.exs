defmodule Aliexpress.Test do
  use ExUnit.Case, async: true

  doctest Aliexpress

  describe "Testing some AliExpress's part" do
    test "basic" do
      part = %Part{ref_provider: "32662451578"}
      part = Aliexpress.init(part)
      part = Aliexpress.request_product(part)
      
      assert part.rohs
      assert part.stock > 0
    end

    test "discount" do
      part = %Part{ref_provider: "4001081336566"}
      part = Aliexpress.init(part)
      part = Aliexpress.request_product(part)
      
      part.quotation |> Enum.map(fn %{discount: discount, origin: origin} ->
        assert discount < origin
      end)
    end
  end
end
