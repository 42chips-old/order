defmodule Digikey.Test do
  use ExUnit.Case, async: true

  doctest Digikey

  describe "Testing some Digi-key's part" do
    test "basic" do
      part = %Part{ref_provider: "PIC32MX130F064B-I%2FSS-ND"}
      part = Digikey.init(part)
      part = Digikey.request_product(part)
      
      assert part.rohs
      assert part.stock > 0
    end
  end
end
