defmodule Provider do
  defstruct [
    name: "Unknown",
    card: []
  ]

  def factorize_parts(%Provider{} = provider) do
    %{provider | card: provider.card
               |> Enum.group_by(fn %{ref_provider: ref} -> ref end)
               |> Enum.map(fn {provider_ref, multipart_by_ref} -> 
                       multipart_by_ref |> Enum.reduce(%Part{
                            ref_provider: provider_ref
                       }, fn (acc_part, part) -> %{
                            acc_part | teams: [acc_part.teams | part.teams]
                       } end)
                  end)
               |> Enum.map(fn part -> Part.factorize_teams(part) end)
    }
  end

  def sum_cost(%Provider{} = provider) do
     provider.card |> Enum.map(fn part -> Part.solve_optimized_price(part) end)
                   |> Enum.map(fn %{origin: cost, askable: qty} -> cost*qty end)
                   |> Enum.sum()
  end

  def nameteams(%Provider{} = provider) do
    provider.card
        |> Enum.map(fn %{teams: teams} ->
                         teams |> Enum.map(fn %{name: name} -> name end)
                    end)
        |> Enum.concat()
        |> Enum.uniq()
  end

  def request_parts(%Provider{} = provider) do
    interface = Enum.join(["Elixir",
                              provider.name |> String.capitalize, "Part"], ".")
                |> String.to_atom
    %{
       provider | card: provider.card
                |> Task.async_stream(fn part -> 
                         part = interface.init(part)
                         interface.request_product(part)
                    end, timeout: :infinity)
                |> Enum.map(fn {:ok, result} -> result end)
    }
  end

  def to_xls_sheet(%Provider{} = provider) do
    nameteams = Provider.nameteams(provider)
    line = Enum.concat(["Prix (TTC)", "Prix Unitaire (TTC)", "Qté"],
                nameteams |> Enum.map(fn t -> Enum.join(["Qté", t], " ") end))
        |> Enum.concat(["RoHS", "Description", "Datasheet",
                "Fabricant", "RefFabricant", "Fournisseur", "RefFournisseur"])
    lines = provider.card
        |> Enum.map(fn part -> Part.to_xls(part, nameteams) end)
    limit = (nameteams |> length)-1

    %Elixlsx.Sheet{
        name: provider.name,
        rows: [line | lines],
        row_heights: %{4 => 60},
        group_cols: [{4..limit+4, collapsed: true}]
    }
  end
end
