defmodule Mouser.Test do
  use ExUnit.Case, async: true

  doctest Mouser

  describe "Testing some Mouser's part" do
    test "basic" do
      part = %Part{ ref_provider: "595-INA290A1IDCKT" }
      part = Mouser.init(part)
      part = Mouser.request_product(part)
      
      assert part.rohs
      assert part.stock > 0
    end
  end
end
