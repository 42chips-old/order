defmodule Order42.CLI do
  @moduledoc false

  use ExCLI.DSL, escript: true, mix_task: :order42

  @name "order42"
  @version Mix.Project.config()[:version]

  name @name
  description "Order42 CLI"
  long_description ~s"""
  This is my long description
  """

  option :version, type: :boolean, aliases: [:V, :v], default: false,
      help: "print program version"
  option :help, type: :boolean, aliases: [:h], default: false,
      help: "give this help list"
  option :minify, type: :boolean, aliases: [:m], default: false,
      help: "specify the XML's cells shouln't be minified"
  option :input, list: true, aliases: [:i], default: [],
      help: "specify the CSV's filename list"
  option :output, aliases: [:o], default: "output.xlsx",
      help: "specify the CSV's filename"

  default_command :program
  command :program do
    aliases [:prog]
    description "Compile a BOM list into an order"
    long_description """
    Gives a nice a warm greeting to whoever would listen
    """

    run context do
      cond do
        context.version -> IO.puts("#{@name} #{@version}")
        context.help -> ExCLI.usage(Order42.CLI) |> IO.puts
        true ->
          # https://github.com/appcues/mojito/issues/49
          {:ok, _} = Application.ensure_all_started(:mojito)

          allpart = context.input
             |> Enum.map(fn filename ->
                            name = filename |> Path.basename(".csv")
                            File.stream!(filename)
                              |> CSV.decode!(headers: true)
                              |> Enum.map(fn out -> Part.init(name, out) end)
                         end)
             |> Enum.concat

          providers = allpart
             |> Enum.group_by(fn %{provider: name} -> name end)
             |> Enum.map(fn {provider, multipart} -> %Provider {
                            name: provider,
                            card: multipart
                        } end)
             |> Enum.map(fn f -> Provider.factorize_parts(f) end)
             |> Enum.map(fn f -> Provider.request_parts(f) end)

          sheet42 = Sheet.model42(providers)
          sheets = providers |> Enum.map(fn f -> Provider.to_xls_sheet(f) end)
          %Elixlsx.Workbook{sheets: [sheet42 | sheets]}
            |> Elixlsx.write_to(context.output)
      end
    end
  end
end
