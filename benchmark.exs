Benchee.run(
  %{
    "Basic Part" => fn ->
      part = %Part{ref_provider: "C5239"}
      part = Lcsc.init(part)
      _part = Lcsc.request_product(part)
    end,
    "Concurrency 10's Part" => fn ->
      parts = ["C33901", "C254096", "C43351", "C440523", "C167536",
               "C353909", "C119894", "C16212", "C393938", "C93522"]
      parts |> Task.async_stream(fn q ->
                   part = %Part{ref_provider: q}
                   part = Lcsc.init(part)
                   _part = Lcsc.request_product(part)
               end, timeout: :infinity)
            |> Enum.map(fn {:ok, result} -> result end)
    end,
  },
  formatters: [
    {Benchee.Formatters.HTML, auto_open: false},
    Benchee.Formatters.Console
  ]
)
