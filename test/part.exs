defmodule Part.Test do
  use ExUnit.Case, async: true

  doctest Part

  describe "basic price optimization part's" do
    test "basic" do
      part = %Part{
        quotation: [%{
            qty: 1,
            origin: 0.14,
        }, %{
            qty: 10,
            origin: 0.10,
        }, %{
            qty: 100,
            origin: 0.09,
        }]
      }
      price = Part.solve_optimized_price(%{
          part | teams: [%{ asked: 7 }]
      })
      assert price.origin == 0.14

      price = Part.solve_optimized_price(%{
          part | teams: [%{ asked: 8 }]
      })
      assert price.origin == 0.10

      price = Part.solve_optimized_price(%{
          part | teams: [%{ asked: 89 }]
      })
      assert price.origin == 0.10

      price = Part.solve_optimized_price(%{
          part | teams: [%{ asked: 91 }]
      })
      assert price.origin == 0.09
    end
  end
end
