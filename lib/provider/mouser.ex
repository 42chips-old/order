defprotocol Mouser do
  def init(part)
  def request_product(part)
end

defimpl Mouser, for: Part do
  @moduledoc """
  represente a Mouser's part.
  """

  @fournisseur "Mouser"
  @fournisseur_link "https://www.mouser.fr"

  def init(%Part{} = part) do
    part = %{part | provider: @fournisseur}
    part = %{part | provider_link: @fournisseur_link}
    part
  end

  def request_product(%Part{} = part) do
    part = %{part | ref_provider_link: part.provider_link <> "/ProductDetail/"
                                                          <> part.ref_provider}
    case Mojito.get(part.ref_provider_link, [{ "User-Agent",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36"
    }]) do
      {:error, %Mojito.Error{message: nil, reason: :timeout}} -> 
         part.ref_provider_link |> IO.puts
      {:ok, response} ->
        {:ok, document} = response |> Map.get(:body)
                                   |> Floki.parse_document

        rohs = document |> Floki.find("i.rohsIconGreen")
        part = %{part | rohs: rohs |> Enum.empty? |> Kernel.!}
                                   
        info = document |> Floki.find("#pdpProdInfo")

        description = info |> Floki.find("#spnDescription")
                           |> Floki.text
                           |> String.trim
        part = %{part | description: description}

        datasheet = info |> Floki.find("#pdp-datasheet_0")
        part = %{part | datasheet: datasheet |> Floki.text |> String.trim}
        part = %{part | datasheet_link: Enum.join([
                               part.provider_link,
                               datasheet |> Floki.attribute("a", "href")
                        ])}

        manufacturer = info |> Floki.find("#lnkManufacturerName")
        part = %{part | manufacturer:
                                manufacturer |> Floki.text |> String.trim}
        part = %{part | manufacturer_link: Enum.join([
                               part.provider_link,
                               manufacturer |> Floki.attribute("a", "href")
                        ])}

        ref_manufacturer = info |> Floki.find("#spnManufacturerPartNumber")
                             |> Floki.text
                             |> String.trim
        part = %{part | ref_manufacturer: ref_manufacturer}

        column = document |> Floki.find("#pdpRightColumn")

        stock =  column |> Floki.find(".pdp-pricing-header")
                        |> Floki.text |> String.trim
                        |> String.replace("En stock: ", "")
        {stock, ""} = stock |> Integer.parse
        part = %{part | stock: stock}

        {quotation, _} = column |> Floki.find(".pdp-pricing-table tr")
               |> Enum.drop(1)
               |> Enum.split_while(fn x -> Floki.find(x, "#reelammohdr")
                                        |> Enum.empty?
                                   end)
        quotation = quotation
               |> Enum.map(fn x -> 
                              {qty, ""} = Floki.find(x, "a") |> Floki.text
                                                             |> Integer.parse
                              {cost, _} =
                                Floki.find(x, "td[headers$='unitpricecolhdr']")
                                    |> Floki.text
                                    |> String.trim
                                    |> String.replace(",", ".")
                                    |> Float.parse

                              %{qty: qty, discount: cost, origin: cost}
                           end)
        part = %{part | quotation: quotation}

        #freight = column
        #        |> Floki.find("[aria-labelledby='factoryLeadTimeLabelHeader']")
        #        |> String.trim
        #part = %{part | freight: freight}
        part
    end
  end
end
