defmodule LcscTest do
  use ExUnit.Case, async: true

  doctest Lcsc

  describe "currency LCSC's" do
    test "get currency" do
      part = %Part{ref_provider: "C5239"}
      part = Lcsc.init(part)
      part = Lcsc.request_currency(part)

      assert part.currency != 0
    end
  end

  describe "Testing some LCSC's part" do
    test "basic" do
      part = %Part{ref_provider: "C5239"}
      part = Lcsc.init(part)
      part = Lcsc.request_product(part)
  
      assert part.ref_provider_link == Enum.join([
        "https://lcsc.com/product-detail/",
        "4000-Series_Texas-Instruments-CD4027BE_C5239.html"
      ])

      assert part.rohs
      assert part.stock > 0
    end
  
    test "discount" do
      part = %Part{ref_provider: "C126666"}
      part = Lcsc.init(part)
      part = Lcsc.request_product(part)
  
      assert part.ref_provider_link == Enum.join([
        "https://lcsc.com/product-detail/",
        "Magnetic-Sensors_Honeywell-VF360NT_C126666",
        ".html"
      ])

      part.quotation |> Enum.map(fn %{discount: discount, origin: origin} ->
        assert discount < origin
      end)
    end
  end
end
