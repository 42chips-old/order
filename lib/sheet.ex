defmodule Sheet do
  @responsable "Alexandre Djivas"
  @pole "42 Chips"

  @currency "#,##0.00 [$€-40C];[RED]-#,##0.00 [$€-40C]"
  @font "Calibri"
  @size 11
  @hsize 36

  alias Elixlsx.Sheet

  def model42(providers) do
    today = Date.utc_today()

    sheet = Sheet.with_name("Feuil1")
      |> Sheet.set_cell("C2", "Date :",
                               size: @size, font: @font, bold: true)
      # Add support for Date - https://github.com/xou/elixlsx/issues/94
      |> Sheet.set_cell("D2", {Date.to_erl(today), {0, 0, 0}}, yyyymmdd: true,
                               align_horizontal: :left,
                               size: @size, font: @font)
      |> Sheet.set_cell("C4", "Pôle :",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_cell("D4", @pole, size: @size, font: @font)
      |> Sheet.set_cell("C5", "Responsable :",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_cell("D5", @responsable,
                               size: @size, font: @font)
      |> Sheet.set_cell("C9", "DEMANDE DE DEPENSE",
                               size: @hsize, font: @font, bold: true)
      |> Sheet.set_cell("B12", "Fournisseur",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_cell("C12", "TTC",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_cell("D12", "Commentaires",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_cell("E12", "Validation 42",
                               size: @size, font: @font, bold: true)
      |> Sheet.set_col_width("A", 8.1)
      |> Sheet.set_col_width("B", 28.45)
      |> Sheet.set_col_width("C", 25.65)
      |> Sheet.set_col_width("D", 40.61)
      |> Sheet.set_col_width("E", 18.59)
      |> Sheet.set_col_width("F", 35.4)
      |> Sheet.set_col_width("G", 21.9)
    
    sheet= 1..100 |> Enum.reduce(sheet, fn (n, acc) ->
               acc |> Sheet.set_row_height(n, 15)
            end)
    #sheet = sheet |> Sheet.set_row_height(9, 370)
    
    limit = providers |> length
    sheet = providers
      |> Enum.zip(1..limit+1)
      |> Enum.zip(13..limit+13)
      |> Enum.reduce(sheet, fn ({{provider, numf}, index}, sheet) ->
           nameteams = Provider.nameteams(provider)
           nameteams = case length(nameteams) do
             1 -> Enum.join(["Équipe", nameteams], " ")
             _ -> Enum.join(["Équipes", Enum.join(nameteams, ", ")], " ")
           end
           name = provider.name
           cost = Provider.sum_cost(provider)

           case rem(numf, 2) do
             1 -> sheet
               |> Sheet.set_cell(Enum.join(["A", index]), numf, size: @size,
                                                                font: @font)
               |> Sheet.set_cell(Enum.join(["B", index]), name, size: @size,
                                                                font: @font)
               |> Sheet.set_cell(Enum.join(["C", index]), cost, size: @size,
                                           num_format: @currency, font: @font)
               |> Sheet.set_cell(Enum.join(["D", index]), nameteams,
                                           size: @size, font: @font)
             0 -> sheet
               |> Sheet.set_cell(Enum.join(["A", index]), numf, size: @size,
                                           bg_color: "#d8d8d8", font: @font)
               |> Sheet.set_cell(Enum.join(["B", index]), name, size: @size,
                                           bg_color: "#d8d8d8", font: @font)
               |> Sheet.set_cell(Enum.join(["C", index]), cost, size: @size,
                                           bg_color: "#d8d8d8", font: @font,
                                           num_format: @currency)
               |> Sheet.set_cell(Enum.join(["D", index]), nameteams,
                                           bg_color: "#d8d8d8", font: @font,
                                           size: @size)
               |> Sheet.set_cell(Enum.join(["E", index]), :empty, size: @size,
                                           bg_color: "#d8d8d8", font: @font)
           end
      end)
    sheet = sheet |> Sheet.set_row_height(9, 40)
    sheet = sheet |> Sheet.set_cell(Enum.join(["C", limit+13]), {
                             :formula, Enum.join(["SUM(C13:C", limit+12, ")"])
                          },
                          num_format: @currency, size: @size, font: @font)
    sheet
  end
end
