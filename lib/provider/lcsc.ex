defprotocol Lcsc do
  def init(part)
  def request_currency(part)
  def request_link_provider(part)
  def request_product(part)
end

defimpl Lcsc, for: Part do
  @moduledoc """
  Request a LCSC's part list.
  """

  @fournisseur "LCSC"
  @fournisseur_link "https://lcsc.com"

  def init(%Part{} = part) do
    part = %{part | provider: @fournisseur}
    part = %{part | provider_link: @fournisseur_link}
    part = Lcsc.request_currency(part)
    part
  end

  @doc """
  Update the LCSC's currency value.

  This function returns a updated currency part.
  ## Examples
  ```
  iex> part = %Part{}
  iex> Lcsc.request_currency(part)
  ```
  """
  def request_currency(%Part{} = part) do
    case Mojito.get(part.provider_link <> "/api/user/isLogin", [
      {"cookie", Enum.join(["currency=eyJpdiI6IlBSTUJTa1BjNVduaEVcL3kyQUhaVU5",
                            "nPT0iLCJ2YWx1ZSI6ImR4NnFYWFRwenloUzBDZTNoUjBLZzR",
                            "1VFhBXC8zSjBJUlV3Mk5JZmF3aENpbG4zY2tVSXpSYTAwKzV",
                            "tUlRQOTI2bkRJa3dyTUhOeXhsSGZxRDhHR21TK0YyR2RCWFF",
                            "KcEVBODVWa25GeTBrTzBWZXFMbkY3R1pFZ2EyWVkxRjM5NzB",
                            "Ha09rdHFCUlliYjFZeFJ2dEFHNXc9PSIsIm1hYyI6ImM3Y2Z",
                            "jMTY1MjZhZGZkOGQ1YmY5NjI2ZDllNzNlMjliMjdiNzhiMjN",
                            "iNjRkNmY1OWM2MDkzMDkzODkzOWMxNmMifQ%3D%3D"])}
    ]) do
      {:ok, response} ->
         user = response |> Map.get(:body)
                         |> Jason.decode!
         currency = user["data"]["currency"]["currency_rate_usd"]
         {currency, ""} = currency |> Float.parse

         part = %{part | currency: currency}
         part
    end
  end

  @doc """
  Initialize the part, update the product link.

  This function returns a copy of part with a updated product page link
  available at ref_provider_link.

  ## Examples
  ```
  iex> part = %Lcsc.Part{ref_provider: "C254342"}
  iex> Lcsc.request_link_provider(part)
  ```
  """
  def request_link_provider(%Part{} = part) do
    case Mojito.get(part.provider_link <> "/api/global/additional/search?q="
                                       <> part.ref_provider) do
      {:ok, response} ->
         case response |> Map.get(:body)
                       |> Jason.decode! do
           %{"result" => %{"lcsc_part_number" => true, "links" => link}, 
             "success" => true} ->
               part = %{part | ref_provider_link: part.provider_link <> link}
               part

      end
    end
  end

  @doc """
  Update the product information.

  This function returns a copy of part with a updated product information
  ## Examples
  ```
  iex> part = %Lcsc.Part{ref_provider: "C254342"}
  iex> part = Lcsc.request_currency(part)
  iex> part = Lcsc.request_link_provider(part)
  iex> Lcsc.request_product(part)
  ```
  """
  def request_product(%Part{} = part) do
    part = Lcsc.request_link_provider(part)
    case Mojito.get(part.ref_provider_link) do
      {:error, %Mojito.Error{message: nil, reason: :timeout}} -> 
         part.ref_provider_link |> IO.puts
      {:ok, response} ->
        {:ok, document} = response |> Map.get(:body)
                                   |> Floki.parse_document
        info = document |> Floki.find("table.info-table td")
                        |> Enum.chunk_every(2)
                        |> Enum.map(fn [{_, _, [label]}, {_, _, value}] ->
                                          {label, value}
                                    end)
        {_, description} = Enum.find(info, &match?({"Description", _}, &1))
        description = description |> Floki.text
        part = %{part | rohs: description |> String.ends_with?(" RoHS")}
        part = %{part | description: description}
        {_, datasheet} = Enum.find(info, &match?({"Datasheet", _}, &1))
        part = %{part | datasheet: datasheet |> Floki.text |> String.trim}
        part = %{part | datasheet_link:
                    datasheet |> Floki.attribute("a", "href") |> Enum.join}
        {_, manufacturer} = Enum.find(info, &match?({" Manufacturer ", _}, &1))
        part = %{part | manufacturer: manufacturer |> Floki.text}
        part = %{part | manufacturer_link: Enum.join([
                               part.provider_link,
                               manufacturer |> Floki.attribute("a", "href")
                        ])}
        {_, ref_manufacturer} = Enum.find(info, &match?({"Mfr.Part #", _}, &1))
        part = %{part | ref_manufacturer: ref_manufacturer |> Floki.text}
        stock = document |> Floki.find(".sz-stock") |> Floki.text
        part = %{part | stock: case stock do
                                  "" -> 0
                                  s -> s |> String.to_integer
                               end}
        quotation = document
            |> Floki.find("table.main-table td")
            |> Enum.chunk_every(3)
            |> Enum.map(fn [qty, cost, _] -> 
                          {qty, ""} = qty |> Floki.attribute("data-value")
                                          |> List.first
                                          |> Integer.parse
                          if cost |> Floki.find("span") |> Enum.empty? do
                            {cost, ""} = cost
                                       |> Floki.text
                                       |> String.replace(~r/[^0-9.]+/, "")
                                       |> Float.parse
                            %{qty: qty, origin: cost * part.currency,
                                        discount: cost * part.currency}
                          else
                            {discount, ""} = cost
                                       |> Floki.find(".discount-price")
                                       |> Floki.text
                                       |> String.replace(~r/[^0-9.]+/, "")
                                       |> Float.parse
                            {origin, ""} = cost
                                       |> Floki.find(".origin-price")
                                       |> Floki.text
                                       |> String.replace(~r/[^0-9.]+/, "")
                                       |> Float.parse
                            %{qty: qty,
                              origin: origin * part.currency,
                              discount: discount * part.currency}
                          end
                        end)
        part = %{part | quotation: quotation}
        part
    end
  end
end

defprotocol LcscSet do
  def handling(provider)
end

defimpl LcscSet, for: Provider do
  @moduledoc """
  Represente the AliExpress's provider.
  """

  @fournisseur "LCSC"
  @fournisseur_link "https://lcsc.com"

  # Handling fee
  # https://lcsc.com/faqs/show?id=12
  #
  # For merchandise total less than$15, the handling fee is charged.
  @fee 2.56
  @limit 13

  def handling(%Provider{} = provider) do
    if Provider.sum_cost(provider) < @limit do
      teams = Provider.nameteams(provider)
            |> Enum.map(fn name -> %{
                           name: name,
                           asked: 1
                       } end)
      %{ provider |
         card: [provider.card | %Part{
           rohs: true,
           description: "handling fee",
           provider: @fournisseur,
           provider_link: @fournisseur_link,
           manufacturer: "LCSC",
           teams: teams,
           quotation: %{
              qty: 1,
              origin: @fee
           }
         }]
      }
    else
      provider
    end
  end
end
