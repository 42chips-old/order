defprotocol Digikey do
  def init(part)
  def request_product(part)
end

defimpl Digikey, for: Part do
  @moduledoc """
  represente a Digi-Key's part.
  """

  @fournisseur "Digi-Key"
  @fournisseur_link "https://www.digikey.fr"

  def init(%Part{} = part) do
    part = %{part | provider: @fournisseur}
    part = %{part | provider_link: @fournisseur_link}
    part
  end

  def request_product(%Part{} = part) do
    part = %{part | ref_provider_link: part.provider_link
                                       <> "/products/fr?keywords="
                                       <> part.ref_provider}
    case Mojito.get(part.ref_provider_link) do
      {:error, %Mojito.Error{message: nil, reason: :timeout}} -> 
         part.ref_provider_link |> IO.puts
      {:ok, response} ->
        {:ok, document} = response |> Map.get(:body)
                                   |> Floki.parse_document
        rohs = document |> Floki.find(".env-and-export-table")
                        |> Floki.find("th:fl-contains('Statut RoHS')")
        part = %{part | rohs: rohs |> Enum.empty? |> Kernel.!}

        overview = document |> Floki.find("#product-overview")

        description = overview |> Floki.find("h3[itemprop='description']")
                               |> Floki.text
                               |> String.trim
        part = %{part | description: description}

        headline = document |> Floki.find(".product-details-headline")
        datasheet = headline |> Floki.find("h1[itemprop='model']")
                             |> Floki.text
                             |> String.trim
        part = %{part | datasheet: datasheet}
        part = %{part | datasheet_link:
                headline |> Floki.attribute("a", "href") |> List.first}

        manufacturer = overview |> Floki.find("h2[itemprop='manufacturer']")
        part = %{part | manufacturer: manufacturer |> Floki.text}
        part = %{part | manufacturer_link: Enum.join([part.provider_link,
                manufacturer |> Floki.attribute("a", "href") |> List.first])}

        ref_provider = overview |> Floki.find("#reportPartNumber")
                                 |> Floki.text
                                 |> String.trim
        part = %{part | ref_provider: ref_provider}

        stock = document |> Floki.find("#dkQty")
                         |> Floki.text
                         |> String.replace(~r/ /, "")
        {stock, ""} = stock |> Integer.parse
        part = %{part | stock: stock}

        quotation = document |> Floki.find("table.product-dollars td")
                  |> Enum.chunk_every(3)
                  |> Enum.map(fn [qty, cost,  _] -> 
                                 {qty, ""} = qty |> Floki.text
                                                 |> Integer.parse
                                 {cost, ""} =
                                       cost |> Floki.text
                                            |> String.replace(",", ".")
                                            |> Float.parse
                                 %{qty: qty, discount: cost, origin: cost}
                              end)
        part = %{part | quotation: quotation}
        part
    end
  end
end
